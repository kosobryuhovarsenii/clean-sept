$(document).ready(function () {
  $(window).resize(function () {
    $(".viewport").attr(
      "content",
      "width=" + (window.screen.width >= 480 ? "1024" : "device-width")
    );
  });

  // реализация меню
  const nav__menu = [
    {
      value: 'О продукте',
      href: 'product'
    },
    {
      value: 'Наш товар',
      href: 'catalog'
    },
    {
      value: 'Магазины-партнеры',
      href: 'partners'
    },
    {
      value: 'Наши клиенты',
      href: 'clients'
    },
    {
      value: 'Документы и сертификаты',
      href: 'document'
    },
    {
      value: 'Форма обратной связи',
      href: 'call'
    },
    {
      value: 'Прайс',
    }
  ]

  $('.nav__menu-info').before("<ul></ul>");
  nav__menu.forEach(element => {
    $("<li><a class='nav__menu-link' href='#" + element.href +"'>" + element.value + "</a></li>").appendTo($(".nav__menu ul"));
  });
  $('.nav__menu-link').on('click', function(){
    $(this).css('color', 'blue')
  })

  // конец менюхи

  $(".catalog__block").slick({
    arrows: false,
    dots: false,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    variableWidth: true,
    touchMove: false,
    swipe: false,
    responsive: [
      {
        breakpoint: 481,
        settings: {
          mobileFirst: true,
          focusOnSelect: true,
          touchMove: true,
          swipe: true,
        },
      },
    ],
  });

  $(".partners__block").slick({
    arrows: false,
    dots: false,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    variableWidth: true,
    touchMove: false,
    swipe: false,
    responsive: [
      {
        breakpoint: 481,
        settings: {
          mobileFirst: true,
          focusOnSelect: true,
          touchMove: true,
          swipe: true,
        },
      },
    ],
  });

  $(".clients-carousel").slick({
    infinite: true,
    arrow: true,
    prevArrow: ".clients-carousel__prev",
    nextArrow: ".clients-carousel__next",
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 481,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  });

  $(".document-carousel").slick({
    infinite: true,
    arrow: true,
    prevArrow: ".document-carousel__prev",
    nextArrow: ".document-carousel__next",
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 481,
        settings: {
          infinite: false,
          arrow: false,
          dots: false,
          slidesToShow: 1,
          variableWidth: true,
          mobileFirst: true,
        },
      },
    ],
  });

  $("a").click(function (event) {
    // event.preventDefault();
    let elementClick = $(this).attr("href");
    let destination = $(elementClick).offset().top;
    $("html:not(:animated),body:not(:animated)").animate(
      { scrollTop: destination - $(".header").height() },
      1000
    );
    return false;
  });

  $(".nav__menu-link").click(function () {
    $(".nav__btn").click();
  });

  // $(".nav__btn").click(function(){
  //   $(".modal-bg").fadeIn(300);
  // });

  $(".partners__card-button").each(function () {
    let href = $(this).attr("href");
    $(this).on("click", function () {
      console.log("cerf");
      window.open(href);
    });
    $(this).hover(
      function () {
        $(this).siblings().css("width", "100%");
      },
      function () {
        $(this).siblings().css("width", "30%");
      }
    );
  });

  $("input[type=tel]").mask("+7 (999) 999-9999");

  $("#call-form").validate({
    rules: {
      name: {
        required: true,
        minlength: 2,
      },
      phone: "required",
      email: {
        required: true,
        email: true,
      },
    },
    messages: {
      name: "Введите имя",
      phone: "Введите номер",
      email: {
        required: "Введите e-mail",
        email: "Адрес должен быть вида name@domain.ru",
      },
    },
  });

  $("#modal-form").validate({
    rules: {
      name: {
        required: true,
        minlength: 2,
      },
      phone: "required",
      email: {
        required: true,
        email: true,
      },
    },
    messages: {
      name: "Введите имя",
      phone: "Введите номер",
      email: {
        required: "Введите e-mail",
        email: "Адрес должен быть вида name@domain.ru",
      },
    },
  });

  $(".minus").click(function (e) {
    console.log("cerf");
    e.preventDefault();
    var $input = $(this).parent().find("input");
    var count = parseInt($input.val()) - 1;
    count = count < 1 ? 1 : count;
    $input.val(count);
    $input.change();
    return false;
  });

  $(".plus").click(function (e) {
    e.preventDefault();
    var $input = $(this).parent().find("input");
    $input.val(parseInt($input.val()) + 1);
    $input.change();
    return false;
  });

  $("body").on("click", ".modal-btn", function (e) {
    e.preventDefault();
    $(".modal").fadeIn(300);
    $(".modal-bg").fadeIn(300);
  });

  $("body").on("click", ".modal-header__btn", function (e) {
    e.preventDefault();
    $(".modal").fadeOut(300);
    $(".modal-bg").fadeOut(300);
  });

  $("body").on("click", ".modal-bg", function (e) {
    e.preventDefault();
    $(".modal").fadeOut(300);
    $(".modal-bg").fadeOut(300);
  });

  $("body").on("click", ".image", function (e) {
    e.preventDefault();
    let src = $(this).attr("src");
    $("body").append(
      "<div class='popup'>" +
        "<div class='popup_bg'></div>" +
        `<div class='popup-block'>
      <img src='${src}' class='popup_img' />
      <a class='popup-close' href='#'>x</a>
      </div>` +
        "</div>"
    );
    $(".popup").fadeIn(300);
    $(".popup_bg").click(function () {
      $(".popup").fadeOut(300);
    });
    $(".popup-close").click(function (e) {
      e.preventDefault();
      $(".popup").fadeOut(300);
    });
  });
});
